import { combineReducers } from 'redux';
import productReducer from './product/reducers/productReducer';
import profileReducer from './profile/reducers/ProfileReducer'

export default combineReducers({
  product: productReducer,
  profile: profileReducer
});
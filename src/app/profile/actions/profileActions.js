export const getProfileDetails = (path) => (dispatch) => {
  fetch(`http://localhost:8080/api${path}`, {method: 'GET'})
    .then(response => response.json())
    .then((result) => {
      dispatch({
        type: 'GET_PROFILE_DETAILS',
        profileDetails: {name: result.name, gender: result.gender, description: result.description}
      });
    });
};
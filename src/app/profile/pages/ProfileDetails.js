import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getProfileDetails} from "../actions/profileActions"
import Details from "../../profile/components/Details";

class ProfileDetails extends Component {
  componentDidMount() {
    this.props.getProfileDetails(this.props.location.pathname);
  }

  render() {
    const {name, gender, description} = this.props.profileDetails;

    return (
      <div className="">
        <h3>User Profile</h3>
        <Details name={name} gender={gender} description={description} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  profileDetails: state.profile.profileDetails
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getProfileDetails
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProfileDetails);

